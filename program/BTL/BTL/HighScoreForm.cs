﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL
{
  public partial class HighScoreForm : Form
  {
    public HighScoreForm()
    {
      InitializeComponent();
    }

    public HighScoreForm(Data data)
    {
      InitializeComponent();
      //set all lines to empty initialy
      lblName1.Text = "";
      lblScore1.Text = "";
      lblName2.Text = "";
      lblScore2.Text = "";
      lblName3.Text = "";
      lblScore3.Text = "";
      lblName4.Text = "";
      lblScore4.Text = "";
      //display the champion
      if (data.hallOfFame.Count > 0)
      {
        lblName1.Text += "1. " + data.hallOfFame[0].name + "\n\n";
        lblScore1.Text += data.hallOfFame[0].score.ToString() + "\n\n";
      }
      //display the second best player
      if (data.hallOfFame.Count > 1)
      {
        lblName2.Text += "2. " + data.hallOfFame[1].name + "\n\n";
        lblScore2.Text += data.hallOfFame[1].score.ToString() + "\n\n";
      }
      //display the third best player
      if (data.hallOfFame.Count > 2)
      {
        lblName3.Text += "3. " + data.hallOfFame[2].name + "\n\n";
        lblScore3.Text += data.hallOfFame[2].score.ToString() + "\n\n";
      }
      //display the rest
      for (int i = 3; i < data.hallOfFame.Count; i++)
      {
        lblName4.Text += (i + 1) + ". " + data.hallOfFame[i].name + "\n\n";
        lblScore4.Text += data.hallOfFame[i].score.ToString() + "\n\n";
      }
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
