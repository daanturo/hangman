﻿namespace BTL
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.btnReturn = new System.Windows.Forms.Button();
      this.lblDescription = new System.Windows.Forms.Label();
      this.lblWord = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.lblPressed = new System.Windows.Forms.Label();
      this.btnNext = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnReturn
      // 
      this.btnReturn.Location = new System.Drawing.Point(19, 20);
      this.btnReturn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.btnReturn.Name = "btnReturn";
      this.btnReturn.Size = new System.Drawing.Size(112, 35);
      this.btnReturn.TabIndex = 0;
      this.btnReturn.Text = "Return";
      this.btnReturn.UseVisualStyleBackColor = true;
      this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
      // 
      // lblDescription
      // 
      this.lblDescription.AutoSize = true;
      this.lblDescription.Location = new System.Drawing.Point(18, 405);
      this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblDescription.Name = "lblDescription";
      this.lblDescription.Size = new System.Drawing.Size(104, 20);
      this.lblDescription.TabIndex = 1;
      this.lblDescription.Text = "lblDescription";
      // 
      // lblWord
      // 
      this.lblWord.AutoSize = true;
      this.lblWord.Font = new System.Drawing.Font("Comic Sans MS", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblWord.ForeColor = System.Drawing.Color.Black;
      this.lblWord.Location = new System.Drawing.Point(18, 340);
      this.lblWord.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblWord.Name = "lblWord";
      this.lblWord.Size = new System.Drawing.Size(108, 45);
      this.lblWord.TabIndex = 2;
      this.lblWord.Text = "_ _ _";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.lblPressed);
      this.groupBox1.Location = new System.Drawing.Point(350, 20);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.groupBox1.Size = new System.Drawing.Size(207, 300);
      this.groupBox1.TabIndex = 3;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Letters tried";
      // 
      // lblPressed
      // 
      this.lblPressed.AutoSize = true;
      this.lblPressed.Location = new System.Drawing.Point(9, 41);
      this.lblPressed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblPressed.Name = "lblPressed";
      this.lblPressed.Size = new System.Drawing.Size(0, 20);
      this.lblPressed.TabIndex = 0;
      // 
      // btnNext
      // 
      this.btnNext.Enabled = false;
      this.btnNext.Location = new System.Drawing.Point(228, 20);
      this.btnNext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.btnNext.Name = "btnNext";
      this.btnNext.Size = new System.Drawing.Size(112, 35);
      this.btnNext.TabIndex = 4;
      this.btnNext.Text = "Next";
      this.btnNext.UseVisualStyleBackColor = true;
      this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
      // 
      // GameForm
      // 
      this.AcceptButton = this.btnNext;
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(597, 491);
      this.Controls.Add(this.btnNext);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.lblWord);
      this.Controls.Add(this.lblDescription);
      this.Controls.Add(this.btnReturn);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.KeyPreview = true;
      this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.MaximizeBox = false;
      this.Name = "GameForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Hangman";
      this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblWord;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Label lblPressed;
    }
}

