﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL
{
  public partial class NameForm : Form
  {
    public bool canceled = true;
    public NameForm()
    {
      InitializeComponent();
    }

    private void btnOk_Click(object sender, EventArgs e)
    {
      canceled = false;
      this.Hide();
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      this.Hide();
    }

    public string GetText()
    {
      return txtName.Text;
    }
  }
}
