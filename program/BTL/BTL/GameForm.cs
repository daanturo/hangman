﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Media;
using System.IO;

namespace BTL
{
  //game form
  public partial class GameForm : Form
  {
    //index of current word/hint
    private int current = 0;
    //size of the list of words, independant from list.Count
    private int size = 0;
    //the number of key that can be wrongly guessed
    private int lives = 6;
    //current player' score
    private int score = 0;
    //stores used/unused state of letters
    private bool[] used = new bool[26];
    //list of words and hints
    private List<Word_and_Hint> list = new List<Word_and_Hint>();
    //background music
    private SoundPlayer music;

    public GameForm()
    {
      InitializeComponent();
      WordDraw();
    }

    public GameForm(int lvl)
    {
      if (File.Exists("music/battle.wav") && lvl < 3 && Program.Musicon)
      {
        music = new SoundPlayer("music/battle.wav");
        music.PlayLooping();
      }
      if (File.Exists("music/boss.wav") && lvl == 3 && Program.Musicon)
      {
        music = new SoundPlayer("music/boss.wav");
        music.PlayLooping();
      }
      InitializeComponent();
      // lvl is zero-based
      // lvl = 3 => mixed mode
      if (lvl == 3)
      {
        //for each level, append all words to the list
        for (int i = 0; i < 3; i++)
        {
          foreach (Word_and_Hint word in Program.data.level[i])
            list.Add(word);
        }
      }
      else // non-mixed level
      {
        //copy the entire level's word list
        foreach (Word_and_Hint word in Program.data.level[lvl])
          list.Add(word);
      }
      //size is initially equal to list.Count
      this.size = list.Count;
      ChooseWord();
      WordDraw();
    }

    //checks if a string contains a letter, not case sensitive
    private bool FindLetter(string word, char pressed)
    {
      foreach (char chr in word)
      {
        if (Char.ToUpper(pressed) == Char.ToUpper(chr))
        {
          return true;
        }
      }
      return false;
    }

    //pick a random word from the list
    private void ChooseWord()
    {
      Random rand = new Random();
      //randomly choose the index of the word
      current = rand.Next(size);
    }
    private void drawWordOnly()
    {
      lblWord.Text = "";
      foreach (char chr in list[current].word)
      {
        //check if the character has been revealed or not
        bool revealed = used[Char.ToUpper(chr) - 'A'];
        //if it's not, display "_"
        if (!revealed) lblWord.Text += "_ ";
        //else display itself
        else lblWord.Text += Char.ToUpper(chr) + " ";
      }
    }
    //draws the "_ _ _" and its description
    private void WordDraw()
    {
      if (size == 0)
      {
        lblWord.Text = "CONGRATULATION!!!";
        lblDescription.Text = "You have cleared the game.";
        stopPlaying();
      }
      for (int i = 0; i < 26; i++) used[i] = false;
      drawWordOnly();
      lblDescription.Text = list[current].hint;
    }

    private void Form1_Paint(object sender, PaintEventArgs e)
    {
      GameDraw(sender, e);
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (Program.Musicon) music.Stop();
    }

    private void Form1_KeyPress(object sender, KeyPressEventArgs e)
    {
      char letter = e.KeyChar;
      //check if is a letter in the alphabet
      //ignore if it's already pressed or the word is completed
      if (
        !((letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z'))
        || used[Char.ToUpper(letter) - 'A'] == true
        || btnNext.Enabled == true
        )
        return;
      //mark the letter as pressed
      used[Char.ToUpper(letter) - 'A'] = true;
      if (!(FindLetter(list[current].word, letter)))
      {
        lives -= 1;
        lblPressed.Text += "\n\n" + Char.ToUpper(letter);
        //redraw the stick man if a wrong character is guessed
        Invalidate();
      }
      //display the updated word
      drawWordOnly();
      if (!lblWord.Text.Contains("_")) //if completed
      {
        if (size > 0) btnNext.Enabled = true;
        btnNext.Select();
      }
      //out of lives, check for high score
      if (lives <= 0)
      {
        //shows hidden word when lose
        lblWord.Text = (list[current].word).ToUpper();
        // game over and creat player name
        DialogResult dlr = MessageBox.Show("GameOver");
        if (dlr == DialogResult.OK)
        {
          stopPlaying();
        }
      }
    }
    private void stopPlaying()
    {
      //check if there is new high score
      if (Program.Musicon) music.Stop();
      if (Program.data.checkNewHigh(score) <= -1)
      {
        this.Close();
        return;
      }
      if (File.Exists("music/fanfare.wav") && Program.Musicon)
      {
        music = new SoundPlayer("music/fanfare.wav");
        music.Play();
      }
      //pop up a form waiting for the player to enter his/her name
      NameForm prompt = new NameForm();
      prompt.ShowDialog();
      if (prompt.canceled == false)
      {
        //insert the player's name and score to data
        Program.data.insertNewHigh(prompt.GetText(), score);
        //write data down to file
        Program.data.writeDownHighScores();
        prompt.Close();
        //display hall of fame
        HighScoreForm hsf = new HighScoreForm(Program.data);
        hsf.ShowDialog();
        this.Close();
      }
      prompt.Close();
      this.Close();
      if (Program.Musicon) music.Stop();
    }
    private void btnNext_Click(object sender, EventArgs e)
    {
      //add score propotionally to the size of the word
      this.score += list[current].word.Length * 16;
      //remove the current word from the list so that it won't appear again
      list[current] = list[size - 1];
      size -= 1;
      //reset the pressed list
      lblPressed.Text = "";
      //reset available lives
      lives = 6;
      //generate the next word
      ChooseWord();
      //draw it
      WordDraw();
      //disable next word until the player clears current word
      btnNext.Enabled = false;
      //redraw the stick man
      Invalidate();
    }

    //draws the stick figure according to lives
    private void GameDraw(object sender, PaintEventArgs e)
    {
      Graphics g = e.Graphics;
      //vẽ màu đen, độ rộng 3
      Pen p = new Pen(Color.Black, 3);
      //cột dọc
      g.DrawLine(p, 170, 170, 170, 60);
      //thanh ngang
      g.DrawLine(p, 100, 60, 172, 60);
      //thanh treo
      g.DrawLine(p, 100, 59, 100, 80);
      //đế
      g.DrawLine(p, 150, 170, 190, 170);
      switch (lives)
      {
        case 5:
          //đầu
          g.DrawEllipse(p, 91, 80, 18, 18);
          break;
        case 4:
          //đầu
          g.DrawEllipse(p, 91, 80, 18, 18);
          //thân
          g.DrawLine(p, 100, 98, 100, 138);
          break;
        case 3:
          //đầu
          g.DrawEllipse(p, 91, 80, 18, 18);
          //thân
          g.DrawLine(p, 100, 98, 100, 138);
          //tay 1
          g.DrawLine(p, 100, 105, 90, 115);
          break;
        case 2:
          //đầu
          g.DrawEllipse(p, 91, 80, 18, 18);
          //thân
          g.DrawLine(p, 100, 98, 100, 138);
          //tay 1
          g.DrawLine(p, 100, 105, 90, 115);
          //tay 2
          g.DrawLine(p, 100, 105, 110, 115);
          break;
        case 1:
          //đầu
          g.DrawEllipse(p, 91, 80, 18, 18);
          //thân
          g.DrawLine(p, 100, 98, 100, 138);
          //tay 1
          g.DrawLine(p, 100, 105, 90, 115);
          //tay 2
          g.DrawLine(p, 100, 105, 110, 115);
          //chân 1
          g.DrawLine(p, 100, 138, 110, 148);
          break;
        case 0:
          //đầu
          g.DrawEllipse(p, 91, 80, 18, 18);
          //thân
          g.DrawLine(p, 100, 98, 100, 138);
          //tay 1
          g.DrawLine(p, 100, 105, 90, 115);
          //tay 2
          g.DrawLine(p, 100, 105, 110, 115);
          //chân 1
          g.DrawLine(p, 100, 138, 110, 148);
          //chân 2
          g.DrawLine(p, 100, 138, 90, 148);
          break;
        default: break;
      }
    }
  }
}
