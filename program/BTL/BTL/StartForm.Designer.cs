﻿namespace BTL
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.panel1 = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
      this.btnStart = new System.Windows.Forms.Button();
      this.btnQuit = new System.Windows.Forms.Button();
      this.btnScore = new System.Windows.Forms.Button();
      this.MusicOn = new System.Windows.Forms.CheckBox();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.panel2);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(909, 48);
      this.panel1.TabIndex = 0;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.White;
      this.label2.Location = new System.Drawing.Point(691, 9);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(92, 17);
      this.label2.TabIndex = 2;
      this.label2.Text = "Version 1.0.0";
      // 
      // panel2
      // 
      this.panel2.Location = new System.Drawing.Point(0, 42);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(802, 374);
      this.panel2.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(3, 7);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(158, 38);
      this.label1.TabIndex = 1;
      this.label1.Text = "Hangman";
      // 
      // btnStart
      // 
      this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnStart.Location = new System.Drawing.Point(172, 126);
      this.btnStart.Name = "btnStart";
      this.btnStart.Size = new System.Drawing.Size(143, 50);
      this.btnStart.TabIndex = 2;
      this.btnStart.Text = "Start";
      this.btnStart.UseVisualStyleBackColor = true;
      this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
      // 
      // btnQuit
      // 
      this.btnQuit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnQuit.Location = new System.Drawing.Point(292, 241);
      this.btnQuit.Name = "btnQuit";
      this.btnQuit.Size = new System.Drawing.Size(143, 50);
      this.btnQuit.TabIndex = 3;
      this.btnQuit.Text = "Quit";
      this.btnQuit.UseVisualStyleBackColor = true;
      this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
      // 
      // btnScore
      // 
      this.btnScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
      this.btnScore.Location = new System.Drawing.Point(416, 126);
      this.btnScore.Name = "btnScore";
      this.btnScore.Size = new System.Drawing.Size(143, 50);
      this.btnScore.TabIndex = 4;
      this.btnScore.Text = "High Scores";
      this.btnScore.UseVisualStyleBackColor = true;
      this.btnScore.Click += new System.EventHandler(this.btnScore_Click);
      // 
      // MusicOn
      // 
      this.MusicOn.AutoSize = true;
      this.MusicOn.Checked = true;
      this.MusicOn.CheckState = System.Windows.Forms.CheckState.Checked;
      this.MusicOn.Font = new System.Drawing.Font("Comic Sans MS", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.MusicOn.Location = new System.Drawing.Point(651, 64);
      this.MusicOn.Name = "MusicOn";
      this.MusicOn.Size = new System.Drawing.Size(79, 27);
      this.MusicOn.TabIndex = 5;
      this.MusicOn.Text = "Music";
      this.MusicOn.UseVisualStyleBackColor = true;
      this.MusicOn.CheckedChanged += new System.EventHandler(this.MusicOn_CheckedChanged);
      // 
      // StartForm
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.SystemColors.ControlDark;
      this.ClientSize = new System.Drawing.Size(788, 418);
      this.Controls.Add(this.MusicOn);
      this.Controls.Add(this.btnScore);
      this.Controls.Add(this.btnQuit);
      this.Controls.Add(this.btnStart);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Name = "StartForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "StartForm";
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button btnScore;
    private System.Windows.Forms.CheckBox MusicOn;
  }
}

