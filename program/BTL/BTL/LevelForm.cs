﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL
{
  public partial class LevelForm : Form
  {
    private void startGameandReturn(int level)
    {
      GameForm c = new GameForm(level);
      this.Hide();
      c.ShowDialog();
      this.Show();
    }
    public LevelForm()
    {
      InitializeComponent();
    }

    private void btnReturn_Click(object sender, EventArgs e)
    {
      StartForm b = new StartForm();
      this.Hide();
    }

    private void btnLevel1_Click(object sender, EventArgs e)
    {
      // go to GameForm with level 1
      startGameandReturn(0);
    }

    private void btnLevel2_Click(object sender, EventArgs e)
    {
      // go to GameForm with level 2
      startGameandReturn(1);
    }

    private void btnLevel3_Click(object sender, EventArgs e)
    {
      // go to GameForm with level 3
      startGameandReturn(2);
    }

    private void btnMixed_Click(object sender, EventArgs e)
    {
      // go to GameForm with wild mode
      startGameandReturn(3);
    }
  }
}
