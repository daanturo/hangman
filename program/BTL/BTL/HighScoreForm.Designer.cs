﻿namespace BTL
{
    partial class HighScoreForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.lblName1 = new System.Windows.Forms.Label();
      this.btnReturn = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.lblName2 = new System.Windows.Forms.Label();
      this.lblName3 = new System.Windows.Forms.Label();
      this.lblName4 = new System.Windows.Forms.Label();
      this.lblScore4 = new System.Windows.Forms.Label();
      this.lblScore3 = new System.Windows.Forms.Label();
      this.lblScore2 = new System.Windows.Forms.Label();
      this.lblScore1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // lblName1
      // 
      this.lblName1.AutoSize = true;
      this.lblName1.BackColor = System.Drawing.Color.Transparent;
      this.lblName1.Font = new System.Drawing.Font("Comic Sans MS", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblName1.ForeColor = System.Drawing.Color.Gold;
      this.lblName1.Location = new System.Drawing.Point(18, 89);
      this.lblName1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblName1.Name = "lblName1";
      this.lblName1.Size = new System.Drawing.Size(113, 45);
      this.lblName1.TabIndex = 0;
      this.lblName1.Text = "label1";
      // 
      // btnReturn
      // 
      this.btnReturn.Location = new System.Drawing.Point(13, 569);
      this.btnReturn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.btnReturn.Name = "btnReturn";
      this.btnReturn.Size = new System.Drawing.Size(112, 35);
      this.btnReturn.TabIndex = 2;
      this.btnReturn.Text = "Return";
      this.btnReturn.UseVisualStyleBackColor = true;
      this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(286, 33);
      this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(164, 30);
      this.label3.TabIndex = 3;
      this.label3.Text = "High Scores";
      // 
      // lblName2
      // 
      this.lblName2.AutoSize = true;
      this.lblName2.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblName2.ForeColor = System.Drawing.Color.Silver;
      this.lblName2.Location = new System.Drawing.Point(18, 151);
      this.lblName2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblName2.Name = "lblName2";
      this.lblName2.Size = new System.Drawing.Size(99, 39);
      this.lblName2.TabIndex = 4;
      this.lblName2.Text = "label1";
      // 
      // lblName3
      // 
      this.lblName3.AutoSize = true;
      this.lblName3.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblName3.ForeColor = System.Drawing.Color.DarkGoldenrod;
      this.lblName3.Location = new System.Drawing.Point(18, 207);
      this.lblName3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblName3.Name = "lblName3";
      this.lblName3.Size = new System.Drawing.Size(84, 35);
      this.lblName3.TabIndex = 5;
      this.lblName3.Text = "label2";
      // 
      // lblName4
      // 
      this.lblName4.AutoSize = true;
      this.lblName4.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
      this.lblName4.Location = new System.Drawing.Point(18, 259);
      this.lblName4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblName4.Name = "lblName4";
      this.lblName4.Size = new System.Drawing.Size(69, 29);
      this.lblName4.TabIndex = 6;
      this.lblName4.Text = "label4";
      // 
      // lblScore4
      // 
      this.lblScore4.AutoSize = true;
      this.lblScore4.Font = new System.Drawing.Font("Comic Sans MS", 10F, System.Drawing.FontStyle.Bold);
      this.lblScore4.Location = new System.Drawing.Point(638, 259);
      this.lblScore4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblScore4.Name = "lblScore4";
      this.lblScore4.Size = new System.Drawing.Size(69, 29);
      this.lblScore4.TabIndex = 10;
      this.lblScore4.Text = "label4";
      this.lblScore4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblScore3
      // 
      this.lblScore3.AutoSize = true;
      this.lblScore3.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblScore3.ForeColor = System.Drawing.Color.DarkGoldenrod;
      this.lblScore3.Location = new System.Drawing.Point(638, 207);
      this.lblScore3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblScore3.Name = "lblScore3";
      this.lblScore3.Size = new System.Drawing.Size(84, 35);
      this.lblScore3.TabIndex = 9;
      this.lblScore3.Text = "label2";
      this.lblScore3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblScore2
      // 
      this.lblScore2.AutoSize = true;
      this.lblScore2.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblScore2.ForeColor = System.Drawing.Color.Silver;
      this.lblScore2.Location = new System.Drawing.Point(638, 151);
      this.lblScore2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblScore2.Name = "lblScore2";
      this.lblScore2.Size = new System.Drawing.Size(99, 39);
      this.lblScore2.TabIndex = 8;
      this.lblScore2.Text = "label1";
      this.lblScore2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblScore1
      // 
      this.lblScore1.AutoSize = true;
      this.lblScore1.BackColor = System.Drawing.Color.Transparent;
      this.lblScore1.Font = new System.Drawing.Font("Comic Sans MS", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblScore1.ForeColor = System.Drawing.Color.Gold;
      this.lblScore1.Location = new System.Drawing.Point(638, 89);
      this.lblScore1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.lblScore1.Name = "lblScore1";
      this.lblScore1.Size = new System.Drawing.Size(113, 45);
      this.lblScore1.TabIndex = 7;
      this.lblScore1.Text = "label1";
      this.lblScore1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // HighScoreForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.LightSeaGreen;
      this.ClientSize = new System.Drawing.Size(754, 618);
      this.Controls.Add(this.lblScore4);
      this.Controls.Add(this.lblScore3);
      this.Controls.Add(this.lblScore2);
      this.Controls.Add(this.lblScore1);
      this.Controls.Add(this.lblName4);
      this.Controls.Add(this.lblName3);
      this.Controls.Add(this.lblName2);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.btnReturn);
      this.Controls.Add(this.lblName1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.MaximizeBox = false;
      this.Name = "HighScoreForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "HighScoreForm";
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName1;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label lblName2;
    private System.Windows.Forms.Label lblName3;
    private System.Windows.Forms.Label lblName4;
    private System.Windows.Forms.Label lblScore4;
    private System.Windows.Forms.Label lblScore3;
    private System.Windows.Forms.Label lblScore2;
    private System.Windows.Forms.Label lblScore1;
  }
}