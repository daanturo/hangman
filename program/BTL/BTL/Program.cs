﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BTL
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    public static Data data = new Data();
    public static bool Musicon = true;
    [STAThread]
    static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      data.read();
      Application.Run(new StartForm());
    }
  }
}
