﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.IO;

namespace BTL
{
  public partial class StartForm : Form
  {
    public StartForm()
    {
      InitializeComponent();
    }

    private void btnQuit_Click(object sender, EventArgs e)
    {
      //quitting confirmation
      DialogResult dlr = MessageBox.Show("Bạn có muốn thoát chương trình?", "Thông báo", MessageBoxButtons.OKCancel);
      if (dlr == DialogResult.OK) Application.Exit();
    }

    private void btnStart_Click(object sender, EventArgs e)
    {
      //start the level choosing form
      LevelForm a = new LevelForm();
      this.Hide();
      a.ShowDialog();
      this.Show();
    }

    private void btnScore_Click(object sender, EventArgs e)
    {
      //display hall of fame
      this.Hide();
      SoundPlayer music = new SoundPlayer();
      if (File.Exists("music/soothing.wav") && Program.Musicon)
      {
        music = new SoundPlayer("music/soothing.wav");
        music.PlayLooping();
      }
      HighScoreForm hsf = new HighScoreForm(Program.data);
      hsf.ShowDialog();
      this.Show();
      if (Program.Musicon) music.Stop();
    }

    private void MusicOn_CheckedChanged(object sender, EventArgs e)
    {
      Program.Musicon = !Program.Musicon;
    }
  }
}
