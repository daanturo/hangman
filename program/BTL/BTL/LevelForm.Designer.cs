﻿namespace BTL
{
    partial class LevelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.btnLevel1 = new System.Windows.Forms.Button();
      this.btnLevel2 = new System.Windows.Forms.Button();
      this.btnLevel3 = new System.Windows.Forms.Button();
      this.btnReturn = new System.Windows.Forms.Button();
      this.btnMixed = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.panel1.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.panel2);
      this.panel1.Location = new System.Drawing.Point(1, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1069, 48);
      this.panel1.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(3, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(158, 38);
      this.label1.TabIndex = 7;
      this.label1.Text = "Hangman";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.White;
      this.label2.Location = new System.Drawing.Point(764, 9);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(92, 17);
      this.label2.TabIndex = 6;
      this.label2.Text = "Version 1.0.0";
      // 
      // panel2
      // 
      this.panel2.Location = new System.Drawing.Point(0, 51);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(800, 402);
      this.panel2.TabIndex = 1;
      // 
      // btnLevel1
      // 
      this.btnLevel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnLevel1.Location = new System.Drawing.Point(138, 115);
      this.btnLevel1.Name = "btnLevel1";
      this.btnLevel1.Size = new System.Drawing.Size(226, 85);
      this.btnLevel1.TabIndex = 3;
      this.btnLevel1.Text = "Level 1";
      this.btnLevel1.UseVisualStyleBackColor = true;
      this.btnLevel1.Click += new System.EventHandler(this.btnLevel1_Click);
      // 
      // btnLevel2
      // 
      this.btnLevel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnLevel2.Location = new System.Drawing.Point(138, 228);
      this.btnLevel2.Name = "btnLevel2";
      this.btnLevel2.Size = new System.Drawing.Size(226, 85);
      this.btnLevel2.TabIndex = 4;
      this.btnLevel2.Text = "Level 2";
      this.btnLevel2.UseVisualStyleBackColor = true;
      this.btnLevel2.Click += new System.EventHandler(this.btnLevel2_Click);
      // 
      // btnLevel3
      // 
      this.btnLevel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnLevel3.Location = new System.Drawing.Point(138, 341);
      this.btnLevel3.Name = "btnLevel3";
      this.btnLevel3.Size = new System.Drawing.Size(226, 85);
      this.btnLevel3.TabIndex = 5;
      this.btnLevel3.Text = "Level 3";
      this.btnLevel3.UseVisualStyleBackColor = true;
      this.btnLevel3.Click += new System.EventHandler(this.btnLevel3_Click);
      // 
      // btnReturn
      // 
      this.btnReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnReturn.Location = new System.Drawing.Point(10, 537);
      this.btnReturn.Name = "btnReturn";
      this.btnReturn.Size = new System.Drawing.Size(110, 50);
      this.btnReturn.TabIndex = 6;
      this.btnReturn.Text = "Return";
      this.btnReturn.UseVisualStyleBackColor = true;
      this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
      // 
      // btnMixed
      // 
      this.btnMixed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnMixed.Location = new System.Drawing.Point(138, 454);
      this.btnMixed.Name = "btnMixed";
      this.btnMixed.Size = new System.Drawing.Size(226, 85);
      this.btnMixed.TabIndex = 7;
      this.btnMixed.Text = "Mixed";
      this.btnMixed.UseVisualStyleBackColor = true;
      this.btnMixed.Click += new System.EventHandler(this.btnMixed_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.label6);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Location = new System.Drawing.Point(434, 72);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(609, 487);
      this.groupBox1.TabIndex = 8;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Description";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.White;
      this.label6.Location = new System.Drawing.Point(25, 407);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(561, 33);
      this.label6.TabIndex = 3;
      this.label6.Text = "Wild mode, words can have any amount of letters";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.White;
      this.label5.Location = new System.Drawing.Point(25, 294);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(343, 33);
      this.label5.TabIndex = 2;
      this.label5.Text = "Words have at least 7 letters";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.Color.White;
      this.label4.Location = new System.Drawing.Point(25, 181);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(278, 33);
      this.label4.TabIndex = 1;
      this.label4.Text = "Words have 5-6 letters";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.White;
      this.label3.Location = new System.Drawing.Point(25, 68);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(278, 33);
      this.label3.TabIndex = 0;
      this.label3.Text = "Words have 3-4 letters";
      // 
      // LevelForm
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.BackColor = System.Drawing.SystemColors.ControlDark;
      this.ClientSize = new System.Drawing.Size(1071, 616);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btnMixed);
      this.Controls.Add(this.btnReturn);
      this.Controls.Add(this.btnLevel3);
      this.Controls.Add(this.btnLevel2);
      this.Controls.Add(this.btnLevel1);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Name = "LevelForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "LevelForm";
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnLevel1;
        private System.Windows.Forms.Button btnLevel2;
        private System.Windows.Forms.Button btnLevel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnMixed;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
  }
}