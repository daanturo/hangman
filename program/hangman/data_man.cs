using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace hangman
{
    struct Word_and_Hint
    {
        public string word, hint;
        public Word_and_Hint(string _word, string _hint)
        {
            this.word = _word;
            this.hint = _hint;
        }
    }
    struct Pro_Player
    {
        public string name;
        public int score;
        public Pro_Player(string _name, int _score)
        {
            this.name = _name;
            this.score = _score;
        }
    }
    interface IWords
    {
        // read words and their respective hints from data/level<?>.csv, note that the first level is 1
        void readLevelChosen(int levelToRead);
    }
    interface IHiScores
    {
        // read honored players' names and scores from data/hall_of_fame.csv
        void readHiScores();
        // the function below check if a new high score is given
        // the position to insert is returned, else if it's not worthy, return -1
        int checkNewHigh(int score);
        // attempt to insert new player's record
        // false is returned if his/her score is less than the last player on list's score
        // true is returned otherwise
        bool insertNewHigh(string name, int score);
        // write down the current records to data/hall_of_fame.csv
        void writeDownHighScores();
    }
    class Data : IWords, IHiScores
    {
        public List<Word_and_Hint>[] level = new List<Word_and_Hint>[3];
        public List<Pro_Player> hallOfFame = new List<Pro_Player>();
        int maxHallOfFameSize = 8;

        // read everything from data/
        public void read()
        {
            readHiScores();
            for (int i = 1; i <= 3; i++) readLevelChosen(i);
        }
        // Data's constructor
        public Data()
        {
            //if the folder "data" is doesn't exist, create it
            string path = "data";
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            for (int i = 0; i < 3; i++)
            {
                this.level[i] = new List<Word_and_Hint>();
            }
        }

        public void readLevelChosen(int levelToRead)
        {
            string path = "data/level" + levelToRead.ToString() + ".csv";
            // the level intended for display is one-based, while stored data is zero-based
            levelToRead -= 1;
            // stop reading if the file doesn't exist
            if (!File.Exists(path)) return;
            StreamReader file = new StreamReader(path);
            string line = "";
            // read each line
            while ((line = file.ReadLine()) != null)
            {
                List<string> tempList = line.Split(',').ToList();
                //ignore if the format is wrong
                if (tempList.Count != 2) { continue; }
                Word_and_Hint word = new Word_and_Hint(tempList[0], tempList[1]);
                this.level[levelToRead].Add(word);
            }
            file.Close();
        }

        public void readHiScores()
        {
            string path = "data/hall_of_fame.csv";
            // if there is no record, create it and stop reading
            if (!File.Exists(path))
            {
                File.CreateText(path);
                return;
            }
            StreamReader file = new StreamReader(path);
            string line = "";
            // read each line
            while ((line = file.ReadLine()) != null)
            {
                // store up to some top players only
                if (this.hallOfFame.Count > maxHallOfFameSize) break;
                List<string> tempList = line.Split(',').ToList();
                if (tempList.Count != 2) { continue; }
                this.hallOfFame.Add(new Pro_Player(tempList[0], int.Parse(tempList[1])));
            }
            // sort the honored players from high to low according to scores
            for (int i = hallOfFame.Count - 1; i >= 2; i--)
            {
                for (int k = 0; k < i; k++)
                {
                    if (hallOfFame[k].score < hallOfFame[i].score)
                        (hallOfFame[i], hallOfFame[k]) = (hallOfFame[k], hallOfFame[i]);
                }
            }
            file.Close();
        }
        public int checkNewHigh(int score)
        {
            for (int pos = 0; pos < hallOfFame.Count; pos++)
            {
                if (score >= hallOfFame[pos].score) return pos;
            }
            return -1;
        }
        public bool insertNewHigh(string name, int score)
        {
            int pos = checkNewHigh(score);
            if (pos == -1) return false;
            hallOfFame.Insert(pos, new Pro_Player(name, score));
            // pop the the player with lowest score out of the list if Hall of Fame is overflow
            if (hallOfFame.Count > maxHallOfFameSize) hallOfFame.RemoveAt(hallOfFame.Count - 1);
            return true;
        }

        public void writeDownHighScores()
        {
            // create a temporary new record of high scores
            // then use it to overwrite the old one incase the program is terminated
            string path = "data/hall_of_fame.csv";
            string newpath = "data/hall_of_fame.csv.new";
            StreamWriter file = File.CreateText(newpath);
            foreach (Pro_Player player in hallOfFame)
            {
                file.WriteLine(player.name + "," + player.score);
            }
            file.Close();
            // overwrite the old record
            if (File.Exists(path)) File.Delete(path);
            File.Move(newpath, path);
        }
    }
}
