﻿using System;
using System.IO;

namespace hangman
{
    class Program
    {
        static void Main(string[] args)
        {
            Data data = new Data();
            data.read();
            foreach (Pro_Player hi in data.hallOfFame){
                Console.WriteLine($"{hi.name}: {hi.score}");
            }
        }
    }
}
