htmlfile = open ("words.html",'r')
outputfile = open("outputfromhtml",'w')
common3 = open("common3.txt",'r')

wordIndicator = "<a class=\"word dynamictext\""
definitionIndicator = "<div class=\"definition\">"
class Word:
    word=""
    hint=""
    def __init__(self, _word,_hint):
        self.word = _word
        self.hint = _hint
        
for line in common3:
    outputfile.write(line)

for line in htmlfile:
    foundAWord = line.find( wordIndicator ) > -1
    foundADef = line.find(definitionIndicator) > -1
    if ( foundADef or foundAWord):
        # strip 
        str = line [ line.find('>') +1: ]
        # the end number after ':' is not inclusive
        str = str [: str.find('<') ]
        # convert all ',' in a defition to ';' to avoid confusion
        if (foundADef):
            strList = list(str)
            for i in range(len(strList)):
                if strList[i]==',':
                    strList[i]=';'
            str = "".join(strList)
        outputfile.write(str)
        if (foundAWord):
            outputfile.write(',')
        elif (foundADef):
            outputfile.write('\n')

htmlfile.close()
outputfile.close()
common3.close()

inputfile= open("outputfromhtml",'r')
file1 = open('output/level1.csv','w')
file2 = open('output/level2.csv','w')
file3 = open('output/level3.csv','w')

list = []
for line in inputfile:
    strList = line.split(',')
    word = Word(strList[0],strList[1])
    list.append(word)

for i in range( len(list) - 1, 0, -1 ):
    for k in range(i):
        if ( list[i].word < list[k].word ):
            list[i], list[k] = list[k], list[i]

for line in list:
    str = line.word
    
    if (len(str)>=3 and len(str)<=4):
        file1.write( str + "," + line.hint[0].upper() + line.hint[1:] )
    if (len(str)>=5 and len(str)<=6):
        file2.write( str + "," + line.hint[0].upper() + line.hint[1:] )
    if (len(str)>=7):
        file3.write( str + "," + line.hint[0].upper() + line.hint[1:] )
inputfile.close()
file1.close()
file2.close()
file3.close()
        
